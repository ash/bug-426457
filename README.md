# [**Bug 426457**](https://bugs.kde.org/show_bug.cgi?id=426457) reproducer

## Summary

This seems to be affecting new installs, but not all old installations.

The reproducer will quickly setup a Neon environment using the most up to date unstable inside qemu for rapid testing.

## Install pre-requisites

```sudo apt -y install cloud-image-utils qemu qemu-kvm net-tools cloud-init```

## Clone and enter the repository

```git clone git@invent.kde.org:ash/bug-426457.git && cd bug-426457```

## Download Ubuntu cloud image

```wget https://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64.img```

## Use repository neon.txt to generate seed

This creates instructions that the Ubuntu cloudimg will read to setup a KDE Neon instance.
If you have previously done the previous instructions, you can always restart from this step.

```cloud-localds seed.img neon.txt --hostname neon```

## Create linked disk image from cloudimg

This way we do not write to the original cloudimg file and save the need to redownload.

```qemu-img create -f qcow2 -b focal-server-cloudimg-amd64.img neon.img 10G```

## Build Neon disk image with reproducer

```kvm -net nic -net user -hda neon.img -hdb seed.img -m 8G -nographic```

## Launch reproducer

```kvm -net nic -net user -hda neon.img -m 8G -vga qxl```

This should launch a full Neon desktop with Docker etc. pre-configured. Launch `konsole` and try `neondocker`.
